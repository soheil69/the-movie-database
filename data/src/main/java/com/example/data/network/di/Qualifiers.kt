package com.example.data.network.di

import javax.inject.Qualifier

@Qualifier
@kotlin.annotation.Retention(AnnotationRetention.BINARY)
internal annotation class CacheBase

@Qualifier
@kotlin.annotation.Retention(AnnotationRetention.BINARY)
internal annotation class Default

//@Qualifier
//@kotlin.annotation.Retention(AnnotationRetention.BINARY)
//internal annotation class BaseUrl