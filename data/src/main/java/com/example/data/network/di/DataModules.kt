package com.example.data.network.di

import android.content.Context
import com.example.core.api.Network
import com.example.data.TheMovieDatabaseNetwork
import com.example.data.network.CacheInterceptor
import com.example.data.network.TheMovieDatabaseApiInfo
import com.example.data.network.TheMovieDatabaseRestApi
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.readystatesoftware.chuck.ChuckInterceptor
import dagger.Binds
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
internal interface DataModule {

    @Binds
    fun network(theMovieDatabaseNetworkNetwork: TheMovieDatabaseNetwork): Network
}

@Module
internal class NetworkModule {

//    @Provides
//    fun dateFormat(): SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

    @Provides
    @Singleton
    fun okHttpClient(context: Context): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(ChuckInterceptor(context))
//            .addNetworkInterceptor(ConnectivityCheckInterceptor(context))
            .build()
    }

    @Provides
    @CacheBase
    fun cacheBaseOkHttpClient(context: Context, okHttpClient: OkHttpClient): OkHttpClient {
        return okHttpClient.newBuilder()
            .cache(Cache(context.cacheDir, 10L * 1024L * 1024L))
            .addInterceptor(CacheInterceptor(context))
            .build()
    }

    @Provides
    fun gson(): Gson {
        return GsonBuilder()
            .setLenient()
            //.setDateFormat(DATE_FORMAT)
            //.excludeFieldsWithoutExposeAnnotation()
            .create()
    }

    @Provides
    @Singleton
    fun retrofit(
        okHttpClient: OkHttpClient,
        gson: Gson
    ): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .baseUrl(TheMovieDatabaseApiInfo.BASE_URL)
            .client(okHttpClient)
            .build()
    }

    @Provides
    @CacheBase
    fun cacheBaseRetrofit(
        @CacheBase okHttpClient: OkHttpClient,
        retrofit: Retrofit
    ): Retrofit {
        return retrofit.newBuilder()
            .client(okHttpClient)
            .build()
    }

    @Provides
    fun theMovieDatabaseRestApi(retrofit: Retrofit): TheMovieDatabaseRestApi {
        return retrofit.create(TheMovieDatabaseRestApi::class.java)
    }

    @Provides
    @CacheBase
    fun cacheBaseTheMovieDatabaseRestApi(@CacheBase retrofit: Retrofit): TheMovieDatabaseRestApi {
        return retrofit.create(TheMovieDatabaseRestApi::class.java)
    }
}