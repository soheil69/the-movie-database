package com.example.data.network.response

import com.google.gson.annotations.SerializedName

internal data class ErrorResponse (
    @field:SerializedName("IntroductionVideo")
    val statusCode: Int,
    @field:SerializedName("IntroductionVideo")
    val statusMessage: String
)