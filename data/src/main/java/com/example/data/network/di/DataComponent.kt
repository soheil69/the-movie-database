package com.example.data.network.di

import android.content.Context
import com.example.core.api.Network
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        NetworkModule::class,
        DataModule::class
    ]
)
interface DataComponent {

    fun network(): Network

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun context(context: Context): Builder
        fun build(): DataComponent
    }
}