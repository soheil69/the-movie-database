package com.example.data.network

import com.example.data.network.TheMovieDatabaseApiInfo.API_KEY
import com.example.data.network.response.TvShowResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

internal interface TheMovieDatabaseRestApi {

    @GET("/3/tv/popular?api_key=$API_KEY&language=en-US")
    fun getPopularTvShows(
        @Query("page") page: Int,
    ): Single<TvShowResponse>
}