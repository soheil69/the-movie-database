package com.example.data

import com.example.core.api.Network
import com.example.core.model.DataResponse
import com.example.core.model.TvShow
import com.example.core.model.exception.NetworkException
import com.example.core.model.exception.NoInternetException
import com.example.core.model.exception.TimeoutException
import com.example.core.model.exception.UnexpectedResponseException
import com.example.data.mapper.HttpExceptionToNetworkErrorMapper
import com.example.data.mapper.TvShowResponseToTvShowDataResponseMapper
import com.example.data.network.TheMovieDatabaseRestApi
import com.example.data.network.di.CacheBase
import com.google.gson.Gson
import com.google.gson.JsonParseException
import dagger.Lazy
import io.reactivex.rxjava3.core.Single
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
internal class TheMovieDatabaseNetwork @Inject constructor(
    @CacheBase private val restApi: Lazy<TheMovieDatabaseRestApi>,
    private val gson: Gson
) : Network {

    override fun getPopularTvShows(
        page:Int
    ): Single<DataResponse<TvShow>> =
        restApi.get()
            .getPopularTvShows(page)
            .flatMap { tvShowResponse ->
                TvShowResponseToTvShowDataResponseMapper().map(tvShowResponse)
                    .let { Single.just(it) }//?: Single.error(UnexpectedResponseException())
            }.onErrorResumeNext { t ->
                when (t) {
                    is HttpException -> {
                        Single.error(NetworkException(HttpExceptionToNetworkErrorMapper(gson).map(t)))
                    }
                    is JsonParseException -> Single.error(UnexpectedResponseException(t.message))
                    is SocketTimeoutException -> Single.error(TimeoutException(t))
                    is IOException -> Single.error(NoInternetException(t))
                    else -> Single.error(t)
                }
            }

}