package com.example.data.mapper

import com.example.core.model.NetworkError
import com.example.core.util.Mapper
import com.google.gson.Gson
import retrofit2.HttpException

internal class HttpExceptionToNetworkErrorMapper(
    private val gson: Gson
) : Mapper<HttpException, NetworkError> {
    override fun map(from: HttpException): NetworkError {
        val errorResponse = HttpExceptionToErrorResponseMapper(gson).map(from)
        return errorResponse?.let {
            NetworkError(from.code(), from.message(), it.statusCode, it.statusMessage)
        } ?: NetworkError(from.code(), from.message())
    }
}