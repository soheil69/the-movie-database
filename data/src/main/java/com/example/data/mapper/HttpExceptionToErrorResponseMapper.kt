package com.example.data.mapper

import com.example.core.util.Mapper
import com.example.data.network.response.ErrorResponse
import com.google.gson.Gson
import retrofit2.HttpException

internal class HttpExceptionToErrorResponseMapper(
    private val gson: Gson
) : Mapper<HttpException, ErrorResponse?> {

    override fun map(from: HttpException): ErrorResponse? {
        return try {
            gson.fromJson(from.response()?.errorBody()?.string(), ErrorResponse::class.java)
        } finally {
        }
    }
}