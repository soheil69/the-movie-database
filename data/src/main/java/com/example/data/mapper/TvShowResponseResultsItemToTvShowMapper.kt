package com.example.data.mapper

import android.net.Uri
import com.example.core.model.TvShow
import com.example.core.model.exception.UnexpectedResponseException
import com.example.core.util.Mapper
import com.example.data.network.TheMovieDatabaseApiInfo.IMAGE_BACKDROP_SIZE
import com.example.data.network.TheMovieDatabaseApiInfo.IMAGE_BASE_URL
import com.example.data.network.TheMovieDatabaseApiInfo.IMAGE_POSTER_SIZE
import com.example.data.network.response.ResultsItem
import java.net.URI


internal class TvShowResponseResultsItemToTvShowMapper : Mapper<ResultsItem?, TvShow> {
    override fun map(from: ResultsItem?): TvShow {
        from ?: throw  UnexpectedResponseException()
        val id = from.id ?: throw UnexpectedResponseException()
        val name = from.name ?: throw UnexpectedResponseException()
        val originalName = from.originalName ?: throw UnexpectedResponseException()
        val overview = from.overview ?: ""
        val posterUrl = from.posterPath?.let {
            try {
                if (it.isNotBlank())
                    Uri.parse(URI.create(IMAGE_BASE_URL + IMAGE_POSTER_SIZE).toString())
                        .buildUpon().appendEncodedPath(URI.create(it).toString()).build()
                else Uri.EMPTY
            } catch (e: Exception) {
                throw UnexpectedResponseException()
            }
        } ?: Uri.EMPTY
        val backdropUrl = from.backdropPath?.let {
            try {
                if (it.isNotBlank())
                    Uri.parse(URI.create(IMAGE_BASE_URL + IMAGE_BACKDROP_SIZE).toString())
                        .buildUpon().appendEncodedPath(URI.create(it).toString()).build()
                else Uri.EMPTY
            } catch (e: Exception) {
                throw UnexpectedResponseException()
            }
        } ?: Uri.EMPTY

        val voteAverage = from.voteAverage ?: throw UnexpectedResponseException()
        val voteCount = from.voteCount ?: throw UnexpectedResponseException()

        return TvShow(
            id,
            name, originalName,
            overview,
            posterUrl, backdropUrl,
            voteAverage, voteCount,
        )
    }

}
