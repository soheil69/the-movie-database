package com.example.data.mapper

import com.example.core.model.DataResponse
import com.example.core.model.TvShow
import com.example.core.model.exception.UnexpectedResponseException
import com.example.core.util.Mapper
import com.example.data.network.TheMovieDatabaseApiInfo.DEFAULT_PAGE_SIZE
import com.example.data.network.response.TvShowResponse

internal class TvShowResponseToTvShowDataResponseMapper : Mapper<TvShowResponse, DataResponse<TvShow>> {

    override fun map(from: TvShowResponse): DataResponse<TvShow> {
        val offset = from.page?.let { p -> (p - 1) * DEFAULT_PAGE_SIZE }
            ?: throw UnexpectedResponseException()
        val limit = DEFAULT_PAGE_SIZE
        val total = from.totalResults ?: throw UnexpectedResponseException()
        val count = from.results?.size
            ?: if (offset + limit >= total) 0 else throw UnexpectedResponseException()
        val results =
            from.results?.let { it.map { t -> TvShowResponseResultsItemToTvShowMapper().map(t) } }
                ?: emptyList()
        return DataResponse(results, count, total, limit, offset)
    }

}
