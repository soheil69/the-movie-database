package com.example.themoviedatabase.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.themoviedatabase.TheMovieDatabaseViewModelFactory
import com.example.themoviedatabase.activities.main.MainActivity
import com.example.themoviedatabase.di.ActivityScope
import com.example.themoviedatabase.di.ViewModelKey
import com.example.themoviedatabase.fragments.detail.DetailViewModel
import com.example.themoviedatabase.fragments.home.HomeViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal class TheMovieDatabaseModule {
//    @Provides
//    fun getConfigInteractor(setting: Setting, network: Network) =
//        GetConfigInteractor(setting, network)
}

@Module
internal interface ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    fun homeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    fun detailViewModel(viewModel: DetailViewModel): ViewModel

    @Binds
    fun theMovieDatabaseViewModelFactory(viewModelFactory: TheMovieDatabaseViewModelFactory): ViewModelProvider.Factory
}

@Module
internal interface ActivityBuilder {

    @ContributesAndroidInjector(modules = [MainActivityModule::class, MainFragmentBuilder::class])
    @ActivityScope
    fun mainActivity(): MainActivity
}

@Module
internal interface ServiceBuilder