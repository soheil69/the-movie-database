package com.example.themoviedatabase.di.modules

import com.example.themoviedatabase.di.FragmentScope
import com.example.themoviedatabase.fragments.detail.DetailFragment
import com.example.themoviedatabase.fragments.home.HomeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal class MainActivityModule

@Module
internal interface MainFragmentBuilder {

    @ContributesAndroidInjector(modules = [HomeFragmentModule::class, HomeChildFragmentBuilder::class])
    @FragmentScope
    fun homeFragment(): HomeFragment

    @ContributesAndroidInjector(modules = [DetailFragmentModule::class, DetailChildFragmentBuilder::class])
    @FragmentScope
    fun detailFragment(): DetailFragment
}