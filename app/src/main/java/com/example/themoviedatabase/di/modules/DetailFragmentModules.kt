package com.example.themoviedatabase.di.modules

import dagger.Module

@Module
internal interface DetailFragmentModule

@Module
internal interface DetailChildFragmentBuilder