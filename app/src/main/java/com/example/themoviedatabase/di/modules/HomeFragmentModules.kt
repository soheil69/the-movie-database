package com.example.themoviedatabase.di.modules

import dagger.Module

@Module
internal interface HomeFragmentModule

@Module
internal interface HomeChildFragmentBuilder