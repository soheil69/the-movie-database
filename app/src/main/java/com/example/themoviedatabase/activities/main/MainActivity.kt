package com.example.themoviedatabase.activities.main

import android.content.res.Configuration
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.view.get
import androidx.navigation.findNavController
import com.example.themoviedatabase.R
import com.example.themoviedatabase.activities.BaseActivity
import com.example.themoviedatabase.databinding.ActivityMainBinding


class MainActivity : BaseActivity<ActivityMainBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
//            requestWindowFeature(Window.FEATURE_NO_TITLE)

        binding = ActivityMainBinding.inflate(layoutInflater).apply { setContentView(root) }

        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            supportActionBar?.hide()
            findNavController(R.id.main_nav_host_fragment).navigateUp()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        if (isLightTheme())
            menu?.get(0)?.setIcon(R.drawable.ic_baseline_light_mode_24)
        else
            menu?.get(0)?.setIcon(R.drawable.ic_baseline_dark_mode_24)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.nightMode -> toggleNightMode().let { true }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun toggleNightMode() {
        if (isDarkTheme())
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        else if (isLightTheme())
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
    }

    private fun isDarkTheme(): Boolean {
        return resources.configuration.uiMode and
                Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_YES
    }

    private fun isLightTheme(): Boolean {
        return resources.configuration.uiMode and
                Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_NO
    }
}