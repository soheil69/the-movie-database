package com.example.themoviedatabase.activities;

public interface OnBackPressListener {
    boolean onBackPressed();
}
