package com.example.themoviedatabase

import android.app.Application
import com.example.data.network.di.DaggerDataComponent
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class TheMovieDatabaseApplication : Application(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()
        DaggerTheMovieDatabaseComponent.builder()
            .applicationContext(applicationContext)
            .dataComponent(DaggerDataComponent.builder().context(applicationContext).build())
            .build().inject(this)
    }

    override fun androidInjector() = dispatchingAndroidInjector
}