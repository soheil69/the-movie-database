package com.example.themoviedatabase

import android.content.Context
import com.example.data.network.di.AppScope
import com.example.data.network.di.DataComponent
import com.example.themoviedatabase.di.ApplicationContext
import com.example.themoviedatabase.di.modules.ActivityBuilder
import com.example.themoviedatabase.di.modules.ServiceBuilder
import com.example.themoviedatabase.di.modules.TheMovieDatabaseModule
import com.example.themoviedatabase.di.modules.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule

@AppScope
@Component(
    dependencies = [DataComponent::class],
    modules = [
        AndroidInjectionModule::class, TheMovieDatabaseModule::class,
        ViewModelModule::class, ActivityBuilder::class, ServiceBuilder::class
    ]
)
interface TheMovieDatabaseComponent {
    fun inject(application: TheMovieDatabaseApplication)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun applicationContext(@ApplicationContext context: Context): Builder
        fun dataComponent(dataComponent: DataComponent): Builder
        fun build(): TheMovieDatabaseComponent
    }
}