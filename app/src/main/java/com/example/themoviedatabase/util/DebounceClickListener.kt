package com.example.themoviedatabase.util

import android.view.View

class DebounceClickListener(
    private val minimumIntervalInMillis: Long,
    private var onClickListener: ((view: View?) -> Unit)? = null
) : View.OnClickListener {

    private var lastClickTime = 0L
    private var firstClickTime = 0L

    fun setOnClickListener(listener: ((view: View?) -> Unit)?) {
        onClickListener = listener
    }

    override fun onClick(v: View?) {
        onClickListener?.let {
            lastClickTime = System.currentTimeMillis()
            if (lastClickTime - firstClickTime > minimumIntervalInMillis || firstClickTime == 0L) {
                firstClickTime = System.currentTimeMillis()
                it(v)
            }
        }
    }
}