package com.example.themoviedatabase.util

import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent

class JustOnceClickListener(
    owner: LifecycleOwner,
    private val event: Lifecycle.Event,
    private var onClickListener: ((view: View?) -> Unit)? = null
) : View.OnClickListener, LifecycleObserver {

    private var isEnabled = true

    init {
        owner.lifecycle.addObserver(this)
    }

    fun setOnClickListener(listener: ((view: View?) -> Unit)?){
        onClickListener = listener
    }

    override fun onClick(v: View?) {
        onClickListener?.let {
            if (isEnabled) {
                isEnabled = false
                it(v)
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        if (event == Lifecycle.Event.ON_CREATE)
            isEnabled = true
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        if (event == Lifecycle.Event.ON_START)
            isEnabled = true
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        if (event == Lifecycle.Event.ON_RESUME)
            isEnabled = true
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        if (event == Lifecycle.Event.ON_DESTROY)
            isEnabled = true
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        if (event == Lifecycle.Event.ON_STOP)
            isEnabled = true
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        if (event == Lifecycle.Event.ON_PAUSE)
            isEnabled = true
    }
}