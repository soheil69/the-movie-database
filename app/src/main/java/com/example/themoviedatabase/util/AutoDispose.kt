package com.example.themoviedatabase.util

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import javax.inject.Inject

class AutoDispose @Inject constructor() : LifecycleObserver {//lifecycle: Lifecycle

    private val destroyDisposable = CompositeDisposable()
    private val stopDisposable = CompositeDisposable()
    private val pauseDisposable = CompositeDisposable()
    private val tagDisposableMap = mutableMapOf<String, Disposable>()

    fun add(disposable: Disposable, event: Lifecycle.Event, tag: String) {
        tagDisposableMap[tag]?.also {
            destroyDisposable.remove(it)
            stopDisposable.remove(it)
            pauseDisposable.remove(it)
        }
        tagDisposableMap[tag] = disposable
        when (event) {
            Lifecycle.Event.ON_DESTROY -> destroyDisposable.add(disposable)
            Lifecycle.Event.ON_STOP -> stopDisposable.add(disposable)
            Lifecycle.Event.ON_PAUSE -> pauseDisposable.add(disposable)
            else -> throw IllegalAccessError("event must be a valid Lifecycle.Event(ON_PAUSE, ON_STOP, ON_DESTROY)")
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        tagDisposableMap.clear()
        destroyDisposable.clear()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() = stopDisposable.clear()

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() = pauseDisposable.clear()
}

fun Disposable.autoDispose(autoDispose: AutoDispose, event: Lifecycle.Event, tag: String) {
    autoDispose.add(this, event, tag)
}