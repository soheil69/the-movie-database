package com.example.themoviedatabase.adapters.tvshow

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.core.model.TvShow
import com.example.themoviedatabase.R
import com.example.themoviedatabase.databinding.AdapterTvShowBinding

class TvShowViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.adapter_tv_show, parent, false)
) {
    private val binding = AdapterTvShowBinding.bind(itemView)
    private val roundOffset = parent.resources.getDimensionPixelOffset(R.dimen.round_offset)

    fun bind(
        item: TvShow?,
        position: Int,
        itemClickListener: ((view: View?, tvShow: TvShow, position: Int) -> Unit)?,
        removeClickListener: ((itemView: View?, tvShow: TvShow, position: Int) -> Unit)?,
        duplicateClickListener: ((itemView: View?, tvShow: TvShow, position: Int) -> Unit)?
    ) {
        item ?: return
        itemClickListener?.let {
            itemView.setOnClickListener { view -> it(view, item, position) }
        } ?: itemView.setOnClickListener(null)

        binding.duplicateBtn.setOnClickListener {
            duplicateClickListener?.invoke(it, item, position)
        }
        binding.removeBtn.setOnClickListener {
            removeClickListener?.invoke(it, item, position)
        }
        Glide.with(itemView.context)
            .load(item.posterUrl)
            .placeholder(R.drawable.popular_tv_show_colored)
            .error(R.drawable.no_poster_party_colored)
            .centerCrop()
            .apply(RequestOptions.bitmapTransform(RoundedCorners(roundOffset)))
            .into(binding.posterImg)
    }
}