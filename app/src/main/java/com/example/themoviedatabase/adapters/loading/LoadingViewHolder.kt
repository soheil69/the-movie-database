package com.example.themoviedatabase.adapters.loading

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.example.themoviedatabase.R
import com.example.themoviedatabase.databinding.AdapterLoadingBinding

class LoadingViewHolder(parent: ViewGroup, retry: () -> Unit) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.adapter_loading, parent, false)
) {
    private val binding = AdapterLoadingBinding.bind(itemView).apply {
        retryBtn.setOnClickListener { retry() }
    }

    fun bind(loadState: LoadState) {
        when (loadState) {
            is LoadState.Error -> {
                binding.errorMsg.text = loadState.error.message // Todo(Use ExceptionMessageFactory)
                if (binding.loading.isShimmerStarted) binding.loading.stopShimmer()
                binding.loading.visibility = View.GONE
                binding.retryBtn.visibility = View.VISIBLE
                binding.errorMsg.visibility = View.VISIBLE
            }
            is LoadState.Loading -> {
                binding.retryBtn.visibility = View.GONE
                binding.errorMsg.visibility = View.GONE
                binding.loading.visibility = View.VISIBLE
                if (!binding.loading.isShimmerStarted) binding.loading.startShimmer()
            }
            is LoadState.NotLoading -> {
                binding.retryBtn.visibility = View.GONE
                binding.errorMsg.visibility = View.GONE
                if (binding.loading.isShimmerStarted) binding.loading.stopShimmer()
                binding.loading.visibility = View.GONE
            }
        }
    }
}