package com.example.themoviedatabase.adapters.tvshow

import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.example.core.model.TvShow

class TvShowAdapter :
    PagingDataAdapter<TvShow, TvShowViewHolder>(object : DiffUtil.ItemCallback<TvShow>() {
        override fun areItemsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
            return oldItem == newItem
        }
    }) {

    companion object {
        const val LOADING_ITEM = 0
        const val TV_SHOW_ITEM = 1
    }

    private var removeClickListener: ((itemView: View?, tvShow: TvShow, position: Int) -> Unit)? =
        null

    fun setOnRemoveClickListener(listener: ((itemView: View?, tvShow: TvShow, position: Int) ->
    Unit)
    ?) {
        this.removeClickListener = listener
    }

    private var duplicateClickListener: ((itemView: View?, tvShow: TvShow, position: Int) ->
    Unit)? =
        null

    fun setOnDuplicateClickListener(listener: ((itemView: View?, tvShow: TvShow, position: Int) ->
    Unit)
    ?) {
        this.duplicateClickListener = listener
    }

    private var itemClickListener: ((itemView: View?, tvShow: TvShow, position: Int) -> Unit)? =
        null

    fun setOnItemClickListener(listener: ((itemView: View?, tvShow: TvShow, position: Int) -> Unit)?) {
        this.itemClickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        TvShowViewHolder(parent)

    override fun onBindViewHolder(holder: TvShowViewHolder, position: Int) =
        holder.bind(getItem(position), position, itemClickListener, removeClickListener, duplicateClickListener)

    override fun getItemViewType(position: Int): Int {
        return if (position == itemCount) TV_SHOW_ITEM else LOADING_ITEM
    }
}