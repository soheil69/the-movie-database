package com.example.themoviedatabase.fragments.detail

import com.example.core.model.TvShow

sealed class DetailFragmentState {
    object ShowInitialViewState : DetailFragmentState()
    data class ShowTvShowViewState(val tvShow: TvShow) : DetailFragmentState()
}