package com.example.themoviedatabase.fragments.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.core.model.TvShow
import com.example.themoviedatabase.R
import com.example.themoviedatabase.databinding.FragmentDetailBinding
import com.example.themoviedatabase.fragments.BaseFragment

class DetailFragment : BaseFragment<FragmentDetailBinding>() {

    private val detailViewModel by lazy {
        ViewModelProvider(requireActivity(), viewModelFactory)[DetailViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentDetailBinding.inflate(inflater, container, false)
        .let { binding = it; binding.root }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        detailViewModel.detailFragmentState.observe(viewLifecycleOwner) { render(it) }
    }

    override fun onBackPressed(): Boolean {
        detailViewModel.setState(DetailFragmentState.ShowInitialViewState)
        return super.onBackPressed()
    }

    private fun render(state: DetailFragmentState) {
        when (state) {
            is DetailFragmentState.ShowInitialViewState -> showInitialView()
            is DetailFragmentState.ShowTvShowViewState -> showTvShowView(state.tvShow)
        }
    }

    private fun showInitialView() {
        binding.primaryInfo.detailGroup.visibility = View.GONE
        binding.primaryInfo.messageTxt.visibility = View.VISIBLE
        binding.primaryInfo.guideTxt.visibility = View.VISIBLE
    }

    private fun showTvShowView(tvShow: TvShow) {
        binding.primaryInfo.detailGroup.visibility = View.VISIBLE
        binding.primaryInfo.messageTxt.visibility = View.GONE
        binding.primaryInfo.guideTxt.visibility = View.GONE

        binding.primaryInfo.nameTxt.text = if (tvShow.name == tvShow.originalName) tvShow.name
        else getString(R.string.name_format).format(tvShow.name, tvShow.originalName)
        binding.primaryInfo.overviewTxt.text = tvShow.overview
        binding.primaryInfo.rating.rating = tvShow.voteAverage.toFloat()
        binding.primaryInfo.rateTxt.text = getString(R.string.rating_format)
            .format(tvShow.voteAverage, tvShow.voteCount)

        Glide.with(this).load(tvShow.backdropUrl)
            .placeholder(R.drawable.television_broadcast)
            .error(R.drawable.television_broadcast)
            .into(binding.primaryInfo.backdropImage)
    }
}