package com.example.themoviedatabase.fragments.home

sealed class HomeFragmentState {
    object ShowInitialViewState : HomeFragmentState()
    object ShowRecyclerViewState : HomeFragmentState()
}