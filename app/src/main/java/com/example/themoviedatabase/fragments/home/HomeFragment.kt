package com.example.themoviedatabase.fragments.home

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.GridLayoutManager
import com.example.core.model.TvShow
import com.example.themoviedatabase.R
import com.example.themoviedatabase.adapters.loading.LoadingAdapter
import com.example.themoviedatabase.adapters.tvshow.TvShowAdapter
import com.example.themoviedatabase.databinding.FragmentHomeBinding
import com.example.themoviedatabase.fragments.BaseFragment
import com.example.themoviedatabase.fragments.detail.DetailFragmentState
import com.example.themoviedatabase.fragments.detail.DetailViewModel
import com.example.themoviedatabase.util.DebounceClickListener
import com.example.themoviedatabase.util.withLoadStateAll
import com.example.themoviedatabase.views.GridItemDecoration
import kotlinx.coroutines.ExperimentalCoroutinesApi

class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>(HomeViewModel::class.java) {

    private val detailViewModel by lazy {
        ViewModelProvider(requireActivity(), viewModelFactory)[DetailViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentHomeBinding.inflate(inflater, container, false)
        .let { binding = it; binding.root }

    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val state = detailViewModel.detailFragmentState.value
        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT
            && state is DetailFragmentState.ShowTvShowViewState
        ) {
            findNavController().navigate(
                HomeFragmentDirections.actionHomeFragmentToDetailFragment(state.tvShow)
            )
            return
        }
        setupView()
        viewModel.homeFragmentState.observe(viewLifecycleOwner) { render(it) }
        viewModel.pagingData.observe(viewLifecycleOwner) {
            setupAdapterAndRecyclerView(it)
        }
    }

    override fun onBackPressed(): Boolean {
        return super.onBackPressed() ||
                (detailViewModel.detailFragmentState.value != DetailFragmentState.ShowInitialViewState).also {
                    if (it) detailViewModel.setState(DetailFragmentState.ShowInitialViewState)
                } ||
                (viewModel.homeFragmentState.value != HomeFragmentState.ShowInitialViewState).also {
                    if (it) viewModel.setState(HomeFragmentState.ShowInitialViewState)
                }
    }

    override fun onDestroyView() {
        binding.tvShowRecyclerView.adapter = null
        super.onDestroyView()
    }

    @ExperimentalCoroutinesApi
    private fun setupView() {
        binding.tvShowRecyclerView.addItemDecoration(
            GridItemDecoration(
                resources.getInteger(R.integer.span_count),
                resources.getDimensionPixelOffset(R.dimen.space_offset),
                true
            )
        )
        binding.startBtn.setOnClickListener(DebounceClickListener(1000) {
            viewModel.start()
            viewModel.setState(HomeFragmentState.ShowRecyclerViewState)
            detailViewModel.setState(DetailFragmentState.ShowInitialViewState)
        })
    }

    private fun render(state: HomeFragmentState) {
        when (state) {
            is HomeFragmentState.ShowInitialViewState -> showInitialView()
            is HomeFragmentState.ShowRecyclerViewState -> showRecyclerView()
        }
    }

    private fun showInitialView() {
        binding.initialViews.visibility = View.VISIBLE
        binding.popularTvShowsText.visibility = View.GONE
        binding.tvShowRecyclerView.visibility = View.GONE
    }

    private fun showRecyclerView() {
        binding.initialViews.visibility = View.GONE
        binding.tvShowRecyclerView.visibility = View.VISIBLE
    }

    private fun setupAdapterAndRecyclerView(pagingData: PagingData<TvShow>) {
        val spanCount = resources.getInteger(R.integer.span_count)
        val gridLayoutManager =
            GridLayoutManager(requireContext(), spanCount, GridLayoutManager.VERTICAL, false)
        binding.tvShowRecyclerView.layoutManager = gridLayoutManager

        val tvShowAdapter = TvShowAdapter().apply {
            val clickListener = DebounceClickListener(1000)
            setOnItemClickListener { v, tvShow, _ -> // view, teacher, position
                clickListener.setOnClickListener {
                    detailViewModel.setState(DetailFragmentState.ShowTvShowViewState(tvShow))
                    if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT)
                        findNavController().navigate(
                            HomeFragmentDirections.actionHomeFragmentToDetailFragment(tvShow)
                        )
                }
                clickListener.onClick(v)
            }
            val removeListener = DebounceClickListener(1000)
            setOnRemoveClickListener { v, tvShow, _ -> // view, teacher, position
                removeListener.setOnClickListener {
                    viewModel.remove(tvShow)
                }
                removeListener.onClick(v)
            }
            val duplicateListener = DebounceClickListener(1000)
            setOnDuplicateClickListener { v, tvShow, _ -> // view, teacher, position
                duplicateListener.setOnClickListener {
                    viewModel.duplicate(tvShow)
                }
                duplicateListener.onClick(v)
            }


            addLoadStateListener { combinedLoadStates ->
                when (combinedLoadStates.refresh) {
                    is LoadState.NotLoading -> {
                        if (viewModel.homeFragmentState.value == HomeFragmentState.ShowRecyclerViewState)
                            binding.popularTvShowsText.visibility = View.VISIBLE
                    }
                    is LoadState.Loading -> {
                        binding.popularTvShowsText.visibility = View.GONE
                    }
                    is LoadState.Error -> {
                        binding.popularTvShowsText.visibility = View.GONE
                    }
                }
            }
        }
        binding.tvShowRecyclerView.adapter = tvShowAdapter.withLoadStateAll(
            refresh = LoadingAdapter(tvShowAdapter::refresh),
            header = LoadingAdapter(tvShowAdapter::retry),
            footer = LoadingAdapter(tvShowAdapter::retry)
        )
        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (tvShowAdapter.getItemViewType(position) == TvShowAdapter.LOADING_ITEM) 1 else spanCount
            }
        }
        tvShowAdapter.submitData(viewLifecycleOwner.lifecycle, pagingData)
    }
}