package com.example.themoviedatabase.fragments.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class DetailViewModel @Inject constructor(): ViewModel() {

    private val _detailFragmentState: MutableLiveData<DetailFragmentState> =
        MutableLiveData(DetailFragmentState.ShowInitialViewState)
    val detailFragmentState: LiveData<DetailFragmentState>
        get() = _detailFragmentState

    fun setState(state: DetailFragmentState) {
        _detailFragmentState.postValue(state)
    }
}