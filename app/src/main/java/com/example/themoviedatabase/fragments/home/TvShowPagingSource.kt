package com.example.themoviedatabase.fragments.home

import androidx.paging.PagingState
import androidx.paging.rxjava3.RxPagingSource
import com.example.core.api.Network
import com.example.core.model.DataResponse
import com.example.core.model.TvShow
import com.example.core.model.exception.NetworkException
import com.example.core.model.exception.NoInternetException
import com.example.core.model.exception.TimeoutException
import com.example.core.model.exception.UnexpectedResponseException
import com.example.data.network.TheMovieDatabaseApiInfo
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers

class TvShowPagingSource constructor(
    private val network: Network,
) : RxPagingSource<Int, TvShow>() {

    override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, TvShow>> {
        return network.getPopularTvShows(
            page = params.key?: 1
        )
            .map { toLoadResultPage(it, params) }
            .subscribeOn(Schedulers.io())
            .onErrorResumeNext {
                when (it) {
                    is TimeoutException,
                    is NoInternetException, is NetworkException,
                    is UnexpectedResponseException -> Single.just(LoadResult.Error(it))
                    else -> Single.error(it)
                }
            }
    }

    private fun toLoadResultPage(
        response: DataResponse<TvShow>,
        params: LoadParams<Int>
    ): LoadResult<Int, TvShow> {
        val page = params.key ?: 1
        return LoadResult.Page(
            response.results!!,
            null, // if (page <= 0) null else page - 1,
            if (response.count ?: 0 < TheMovieDatabaseApiInfo.DEFAULT_PAGE_SIZE) null else page + 1
        )
    }

    override fun getRefreshKey(state: PagingState<Int, TvShow>): Int? {
        return null //state.anchorPosition?.let { state.closestPageToPosition(it) }?.nextKey
    }
}