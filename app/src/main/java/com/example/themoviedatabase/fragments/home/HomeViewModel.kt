package com.example.themoviedatabase.fragments.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import androidx.paging.rxjava3.cachedIn
import androidx.paging.rxjava3.flowable
import com.example.core.api.Network
import com.example.core.model.TvShow
import com.example.data.network.TheMovieDatabaseApiInfo
import io.reactivex.rxjava3.disposables.Disposable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val network: Network
) : ViewModel() {

    private val _homeFragmentState: MutableLiveData<HomeFragmentState> =
        MutableLiveData(HomeFragmentState.ShowInitialViewState)
    val homeFragmentState: LiveData<HomeFragmentState>
        get() = _homeFragmentState

    private var tvShowPagerDisposable: Disposable? = null
    private val _pagingData: MutableLiveData<PagingData<TvShow>> = MutableLiveData()
    val pagingData: LiveData<PagingData<TvShow>>
        get() = _pagingData


    @ExperimentalCoroutinesApi
    fun start() {
        tvShowPagerDisposable?.dispose()
        tvShowPagerDisposable = Pager(
            PagingConfig(TheMovieDatabaseApiInfo.DEFAULT_PAGE_SIZE)
        ) { TvShowPagingSource(network) }
            .flowable
            .cachedIn(viewModelScope)
            .subscribe { _pagingData.postValue(it) }
    }

    fun remove(tvShow:TvShow){
        val pages = pagingData.value?:return
            pages.filter { tvShow != it }
            .let { _pagingData.postValue(it) }
    }

    fun duplicate(tvShow:TvShow){
        val pages = pagingData.value?:return
        pages
            .flatMap {
                return@flatMap if (it == tvShow) listOf(it, it.copy())
                else listOf(it)
            }
            .let { _pagingData.postValue(it) }
    }

    override fun onCleared() {
        tvShowPagerDisposable?.dispose()
        super.onCleared()
    }

    fun setState(state: HomeFragmentState) {
        _homeFragmentState.postValue(state)
    }

}