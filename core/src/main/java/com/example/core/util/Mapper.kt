package com.example.core.util

interface Mapper<F, T> {
    fun map(from: F): T
}
