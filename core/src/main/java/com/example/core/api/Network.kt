package com.example.core.api

import com.example.core.model.DataResponse
import com.example.core.model.TvShow
import io.reactivex.rxjava3.core.Single

interface Network {

    fun getPopularTvShows(page:Int): Single<DataResponse<TvShow>>
}