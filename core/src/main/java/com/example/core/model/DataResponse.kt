package com.example.core.model

data class DataResponse<T>(
    val results: List<T>?,
    val count: Int? = null,
    val total: Int? = null,
    val limit: Int? = null,
    val offset: Int? = null
)
