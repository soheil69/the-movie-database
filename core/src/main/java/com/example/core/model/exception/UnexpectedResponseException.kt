package com.example.core.model.exception

class UnexpectedResponseException(
    message: String? = null,
    cause: Throwable? = null
) : RuntimeException(
    message ?: "Unexpected response, probably due to api changes in our server",
    cause
)