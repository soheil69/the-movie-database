package com.example.core.model

data class NetworkError(
    val httpStatusCode: Int,
    val httpStatusMessage: String,
    val statusCode: Int = httpStatusCode,
    val statusMessage: String = httpStatusMessage
)