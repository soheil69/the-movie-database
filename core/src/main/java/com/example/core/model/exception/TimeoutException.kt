package com.example.core.model.exception

import java.io.IOException

class TimeoutException(
    cause: Throwable? = null
) : IOException("Looks like the server is taking to long to respond, this can be caused by either poor connectivity or an error with our servers. Please try again in a while", cause)