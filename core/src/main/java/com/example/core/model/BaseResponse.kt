package com.example.core.model

data class BaseResponse<T>(
    val httpStatusCode: Int,
    val httpStatusMessage: String,
    val statusCode: Int? = null,
    val statusMessage: String? = null,
    val data: DataResponse<T>?
)
