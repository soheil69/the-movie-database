package com.example.core.model

import android.net.Uri
import android.os.Parcel
import android.os.Parcelable

data class TvShow(
    val id: Int,
    val name: String,
    val originalName: String,
    val overview: String,
    val posterUrl: Uri,
    val backdropUrl: Uri,
    val voteAverage: Double,
    val voteCount: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readParcelable(Uri::class.java.classLoader)!!,
        parcel.readParcelable(Uri::class.java.classLoader)!!,
        parcel.readDouble(),
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeString(originalName)
        parcel.writeString(overview)
        parcel.writeParcelable(posterUrl, flags)
        parcel.writeParcelable(backdropUrl, flags)
        parcel.writeDouble(voteAverage)
        parcel.writeInt(voteCount)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TvShow> {
        override fun createFromParcel(parcel: Parcel): TvShow {
            return TvShow(parcel)
        }

        override fun newArray(size: Int): Array<TvShow?> {
            return arrayOfNulls(size)
        }
    }
}