package com.example.core.model.exception

import com.example.core.model.NetworkError
import java.lang.RuntimeException

class NetworkException(
    val networkError: NetworkError,
    cause: Throwable? = null
) : RuntimeException(networkError.statusMessage, cause)