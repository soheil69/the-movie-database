package com.example.core.model.exception

import java.io.IOException

class NoInternetException(
    cause: Throwable? = null
) : IOException("No internet available, please check your WiFi or Data connection", cause)